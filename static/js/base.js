function onReady(callback) {
    var intervalId = window.setInterval(function() {
      if (document.getElementsByTagName('body')[0] !== undefined) {
        window.clearInterval(intervalId);
        callback(this);
      }
    }, 1000);
  }
  
  function setVisible(selector, visible) {
    document.querySelector(selector).style.display = visible ? 'block' : 'none';
  }
  
  onReady(function() {
    setVisible('#wrapper', true);
    setVisible('.load', false);
  });


var modal = document.getElementById('myModal');
var btn = document.getElementById("myBtn");
var span = document.getElementsByClassName("close")[0];
btn.onclick = function() {
    modal.style.display = "flex";
}
span.onclick = function() {
  modal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
var isChanged = false;
function changeTheme(){
        $(".container").toggleClass("orange-back");
        var btn = $(".button")[0];
        console.log(btn);
        for (var i =0; i <3; i = i + 1){ 
            $(".button").toggleClass("dark");
        }
        this.isChanged = true;
        console.log(isChanged);
}

function themePreview(){
        $(".container").toggleClass("orange-back");
        var btn = $(".button")[0];
        console.log(btn);
        for (var i =0; i <3; i = i + 1){ 
            $(".button").toggleClass("dark");
        }
}

function accordion(num){
    $(".accordion" + num).toggleClass("move")   ;
    $(".accordion-content" + num).toggleClass("show");
    $(".plus" + num).toggleClass("min");
}
