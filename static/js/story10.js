var form1 = document
  .getElementById("email_form")
  .addEventListener("input", cekEmail);
var form2 = document
  .getElementById("email_form")
  .addEventListener("change", cekEmail);
var pass1 = document
  .getElementById("password")
  .addEventListener("input", cekPass);

var emailIsValid = false;
var passIsValid = false;
var nameIsNotEmpty = false;

function cekEmail() {
  var emailValue = document.getElementById("email_form").value;
  var emailAlert = document.getElementById("emailAlert");
  var emailNotValid = document.getElementById("emailNotValid");
  var emailValid = document.getElementById("emailValid");
  var emailKosong = document.getElementById("emailKosong");
  emailNotValid.style.display = "none";
  emailAlert.style.display = "none";
  emailValid.style.display = "none";
  emailKosong.style.display = "none";
  if (emailValue != "") {
    $.ajax({
      url: "/story10/",
      data: {
        email: emailValue
      },
      dataType: "json",
      success: function(data) {
        if (
          (emailValue.length <= 10) |
          (emailValue.indexOf("@") == -1) |
          (emailValue.indexOf(" ") != -1) |
          (emailValue.indexOf(".") == -1)
        ) {
          emailNotValid.style.display = "block";
          emailIsValid = false;
        } else if (data.sudah_ada) {
          emailAlert.style.display = "block";
          emailIsValid = false;
        } else {
          emailValid.style.display = "block";
          emailIsValid = true;
        }
      }
    });
  } else {
    emailKosong.style.display = "block";
    emailIsValid = false;
  }
  disableDaftar();
}

function cekName() {
  name = document.getElementById("name_form").value.replace(" ", "");
  var btn = document.getElementById("daftar");
  console.log(name);
  if (name.length > 0) {
    nameIsNotEmpty = true;
  } else {
    nameIsNotEmpty = false;
  }
  console.log(nameIsNotEmpty);
}

function cekPass() {
  var passAlert = document.getElementById("passAlert");
  var passNotValid = document.getElementById("passNotValid");
  var passValid = document.getElementById("passValid");
  var passValue = document.getElementById("password").value;
  passNotValid.style.display = "none";
  passAlert.style.display = "none";
  passValid.style.display = "none";
  if (passValue.length + 1 < 7) {
    passAlert.style.display = "block";
    passIsValid = false;
  } else if (!hasNumber(passValue)) {
    passNotValid.style.display = "block";
    passIsValid = false;
  } else if (isNaN(passValue)) {
    passValid.style.display = "block";
    passIsValid = true;
  } else {
    passNotValid.style.display = "block";
  }
  disableDaftar();
}

function hasNumber(myString) {
  return /\d/.test(myString);
}

function disableDaftar() {
  var btn = document.getElementById("daftar");

  if (emailIsValid && passIsValid) {
    btn.disabled = false;
  } else {
    btn.disabled = true;
  }
}

function daftarPost() {
  var emailFix = document.getElementById("email_form").value;
  var nameFix = document.getElementById("name_form").value;
  var passFix = document.getElementById("password").value;
  var sukses = document.getElementById("success");
  var gagal = document.getElementById("error");
  var emailValid = document.getElementById("emailValid");
  var passValid = document.getElementById("passValid");

  sukses.style.display = "none";
  gagal.style.display = "none";
  if (nameFix.length <= 0) {
    gagal.style.display = "block";
  } else {
    $.ajax({
      type: "post",
      url: "/story10/daftar",
      data: {
        email: emailFix,
        username: nameFix,
        password: passFix,
        csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val()
      },
      success: function() {
        document.getElementById("email_form").value = "";
        document.getElementById("name_form").value = "";
        document.getElementById("password").value = "";
        sukses.style.display = "block";
        gagal.style.display = "none";
        emailValid.style.display = "none";
        passValid.style.display = "none";
        var btn = document.getElementById("daftar");
        btn.disabled = true;
        var container = document.getElementById("data");
        container.style.display = "none";
      },
      error: function() {
        document.getElementById("email_form").value = "";
        document.getElementById("name_form").value = "";
        document.getElementById("password").value = "";
        gagal.style.display = "block";
        emailValid.style.display = "none";
        passValid.style.display = "none";
        var btn = document.getElementById("daftar");
        btn.disabled = true;
      }
    });
  }
}

function showData() {
  var container = document.getElementById("data");
  var nama_email = document.getElementsByClassName("data");
  for (var i = 0; i < nama_email.length; i++) {
    container.removeChild(nama_email[i]);
  }
  $.ajax({
    type: "GET",
    url: "/story10/get_user",
    dataType: "json",
    success: function(data) {
      for (var i = 0; i < data.length; i++) {
        var containerFlex = document.createElement("div");
        var nama = document.createElement("p");
        var email = document.createElement("p");
        var btn = document.createElement("button");
        btn.innerHTML = "hapus";
        console.log(data[i]["slug"]);
        btn.setAttribute("onclick", 'deleteData("' + data[i]["slug"] + '")');
        nama.setAttribute("id", data[i]["slug"]);
        email.setAttribute("id", data[i]["slug"]);
        nama.setAttribute("class", "data");
        email.setAttribute("class", "data");
        containerFlex.setAttribute("class", "data-field");
        nama.innerHTML = "Nama : " + data[i]["nama"];
        email.innerHTML = "Email : " + data[i]["email"];
        containerFlex.append(nama, email, btn);
        container.append(containerFlex);
      }
      container.style.display = "block";
    },
    error: function() {
      alert("gagal :(");
    }
  });
}

function deleteData(slug) {
  var cek = confirm("yakin mau di delete?");
  if (cek == true) {
    $.ajax({
      type: "post",
      url: "/story10/delete/" + slug,
      data: {
        csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val()
      },
      dataType: "json",
      success: function() {
        alert("berhasil dihapus");
        location.reload();
      }
    });
  }
}
