from django.db import models
from django.urls import reverse
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify



# Create your models here.
class UserInfo(models.Model) :
    user = models.OneToOneField(User, on_delete=models.CASCADE) 
    slug = models.SlugField(unique=False)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.email)
        super(UserInfo,self).save(*args,**kwargs)

    def __str__(self) :
        return self.user.username

