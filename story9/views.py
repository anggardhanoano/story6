from django.shortcuts import render,redirect
from django.contrib import messages
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
import requests
from django.contrib.auth import authenticate, login, logout
from story9.models import UserInfo
from django.contrib.auth.models import User

def elektronik_views(request):
    
    return render(request, "story9.html")

def nembak_data(request):
    data_list = []
    num = []
    response = requests.get("https://enterkomputer.com/api/product/notebook.json")
    data = response.json()

    for dataku in range(1000,1100) :
        data_list.append(data[dataku])
    
    return JsonResponse(data_list, safe=False)

def masuk_views(request) :

    if request.method == "POST" :
        username = request.POST.get("username")     # grab the input from the user
        password = request.POST.get("password")

        request.session['local_password'] = password
        if username == "" or password == "" :
            messages.warning(request, "Please Input Your Username and Password")
            return redirect("story9:index") 
        else :
            user = authenticate(username = username, password = password)   # return true if username and pass verified
            print(user)
            if user :
                if user.is_active :
                    login(request, user)
                    
                    return redirect("story9:index") 
                else :
                    messages.warning(request, "Invalid username or password")
                    return HttpResponse("the username or password are invalid")
            else :
                print("there any user failed login")
                print("username: {} and pass: {}".format(username,password))
                messages.warning(request, "Invalid username or password")
                return redirect("story9:index") 

    else :
        return redirect("story9:index") 


@login_required
def keluar_views(request) :
    logout(request)
    return redirect("story9:masuk")