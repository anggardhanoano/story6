from django.urls import path
from story9.views import elektronik_views, nembak_data, masuk_views, keluar_views

app_name = "story9"

urlpatterns = [
    path("", elektronik_views, name="index"),
    path("get_data", nembak_data, name="nembak"),
    path("masuk", masuk_views, name="masuk"),
    path("keluar", keluar_views, name="keluar"),
]