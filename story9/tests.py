from django.test import TestCase, Client

class TestViews(TestCase):

    def test_url_200(self):
        response = self.client.get("/story9")
        self.assertEqual(response.status_code, 301)

    def test_url_404(self):
        response = self.client.get("/story9hsdga")
        self.assertEqual(response.status_code, 404) 

    def test_template(self):
        self.client = Client()
        response = self.client.get("/story9/")
        self.assertTemplateUsed(response, "story9.html")

    def test_nembak_api(self):
        self.client = Client()
        request = self.client.get("/story9/get_data")
        json = request.content.decode("utf8")
        self.assertIn("{", json)
