from django.db import models
from django.template.defaultfilters import slugify

class UserModel(models.Model):
    email = models.EmailField(max_length=120)
    nama = models.CharField(max_length=100)
    password = models.CharField(max_length=120)
    slug = models.SlugField(unique=False)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.email)
        super(UserModel,self).save(*args,**kwargs)
