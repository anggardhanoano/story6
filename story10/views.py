from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth.models import User
from story10.forms import DaftarForm
from story10.models import UserModel
from django.contrib import messages

def validation_form(request):
    email = request.GET.get('email',None)
    data = {
        'sudah_ada' : UserModel.objects.filter(email__iexact=email).exists()
    }

    return JsonResponse(data, safe=False)

def daftar_form(request):
    form = DaftarForm(request.POST)
    
    if request.method == "POST" :
        if form.is_valid():
            instance = form.save(commit=False)
            instance.set_password = request.POST["password"]
            instance.save()
            print("berhasil")
        else :
            print(form.errors)
    
    return render(request, "story10.html", {
        "form" : DaftarForm,
    })

def retrieve_all(request):
    all_user = UserModel.objects.all()
    data = []
    for i in range(len(all_user)):
        data.append({
            "email" : all_user[i].email,
            "nama"  : all_user[i].nama,
            "slug"  : all_user[i].slug,
        })

    return JsonResponse(data, safe=False)

def delete_user(request, slug_email):

    user_obj = UserModel.objects.get(slug=slug_email)
    isSuccess = False
    if request.method == "POST":
        user_obj.delete()
        isSuccess = True

    data = {
        "sukses" : isSuccess
    }

    return JsonResponse(data, safe=False)

