from django.test import TestCase, Client
from story10.models import UserModel
from story10.forms import DaftarForm

class TestViews(TestCase):

    def test_url_200(self):
        response = self.client.get("/story10")
        self.assertEqual(response.status_code, 301)

    def test_url_404(self):
        response = self.client.get("/story10hsdga")
        self.assertEqual(response.status_code, 404) 

    def test_template(self):
        self.client = Client()
        response = self.client.get("/story10/daftar")
        self.assertTemplateUsed(response, "story10.html")

    def test_nembak_api(self):
        self.client = Client()
        request = self.client.get("/story10/")
        json = request.content.decode("utf8")
        self.assertIn("{", json)

    def setUp(self):
        return UserModel.objects.create(email="lion@gmail.com", nama="test lion", password="halo1234", slug="liongmailcom")
                
    def test_models(self):
        object_model = self.setUp()
        self.assertTrue(isinstance(object_model,UserModel))

    def test_post_data(self):
        data = {
            "email":"test@gmail.com",
            "name":"testing",
            "password":"halo123456",
        }
        response = Client().post("/story10/daftar", data)
        self.assertEqual(response.status_code, 200)