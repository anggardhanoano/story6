from django import forms
from story10.models import UserModel


class DaftarForm(forms.ModelForm):
    email = forms.CharField(label="", widget=forms.TextInput(
        attrs = {
                'id' : 'email_form',
                'type' : 'email',
                'name' : 'email',
                }
    ))
    password = forms.CharField(label="", widget=forms.PasswordInput(
    attrs = {
            "id" : "password",
            }
    ))
    username = forms.CharField(label="", widget=forms.TextInput(
    attrs = {
            "id" : "name_form",
            "required": "true",
            }
    ))
    class Meta:
        model = UserModel
        fields = ("email", "password", "username")
