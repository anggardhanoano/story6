from django.urls import path
from story10.views import validation_form, daftar_form, retrieve_all, delete_user

app_name = "story10"

urlpatterns = [
    path("",validation_form, name="validation"),
    path("daftar", daftar_form, name="daftar"),
    path("get_user", retrieve_all, name="retrieve"),
    path("delete/<slug_email>", delete_user, name="")
]