from django.test import TestCase, Client
from lab_06.models import StatusModel
from lab_06.forms import StatusForm
import datetime

# Create your tests here.
class TestModel(TestCase):

    def setUp(self):
        date = datetime.datetime.now()
        return StatusModel.objects.create(email="lion@gmail.com", status="test lion")
                
    def test_models(self):
        object_model = self.setUp()
        self.assertTrue(isinstance(object_model,StatusModel))
        self.assertEqual(object_model.__str__(), object_model.email)


class TestViews(TestCase):
    
    def setUp(self):
        self.client = Client()

    def testCode200(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200, "error")
        self.assertContains(response,"Hello")
        self.assertEqual(len(response.context["status"]), 0)
    
    def testCode404(self):
        response = self.client.get("/halo/")
        self.assertEqual(response.status_code, 404, "error")
        
    def test_valid_data(self):
        form = StatusForm({
            "email":"test@gmail.com",
            "status":"testing",
        })
        self.assertTrue(form.is_valid())
        self.instance = form.save()
        self.assertEqual(self.instance.email, "test@gmail.com")
        self.assertEqual(self.instance.status, "testing")
        
    def test_blank_data(self):
        form = StatusForm({})
        self.assertFalse(form.is_valid())
    
    def test_jual_data(self):
        data = {
            "email":"test@gmail.com",
            "status":"testing",
        }
        response = Client().post("/", data)
        self.assertEqual(response.status_code, 302)
        

class TestChallenge(TestCase):

    def test_url(self):
        self.client = Client()
        response = self.client.get("/biodata/")
        self.assertTemplateUsed(response, "biodata.html")
    
    def test_url_salah(self):
        self.client = Client()
        response = self.client.get("/hahahaha/")
        self.assertEqual(response.status_code, 404, "error")

    def test_html(self):
        self.client = Client()
        response = self.client.get("/biodata/")
        self.assertEqual(response.status_code, 200, "page error")

    def test_contain_name(self):
        response =  Client().get("/biodata/")
        html = response.content.decode("utf8")
        self.assertIn("Anggardha Febriano", html)

    def test_contain_npm(self):
        response =  Client().get("/biodata/")
        html = response.content.decode("utf8")
        self.assertIn("1806235800", html)

    def test_contain_img(self):
        response =  Client().get("/biodata/")
        html = response.content.decode("utf8")
        self.assertIn("<img>", html)

    

        