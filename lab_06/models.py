from django.db import models

class StatusModel(models.Model):
    email = models.EmailField(max_length=200)
    status = models.TextField()
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.email

