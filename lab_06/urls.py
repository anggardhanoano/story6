from django.urls import path
from lab_06.views import Homepage, BioViews

app_name = "home"
 
urlpatterns = [
    path('', Homepage, name='index'),
    path("biodata/", BioViews, name="biodata")
]