from django.shortcuts import render, redirect
from lab_06.models import StatusModel
from lab_06.forms import StatusForm

def Homepage(request):
    form = StatusForm(request.POST)
    if request.method == "POST":
        if form.is_valid():
            instance = form.save()
            return redirect("home:index")
    return render(request, "homepage.html", {
        'form':StatusForm(),
        'status':StatusModel.objects.order_by("-date"),
    })

def BioViews(request):
    return render(request, "biodata.html")

