from django import forms
from lab_06.models import StatusModel

class StatusForm(forms.ModelForm):
    email = forms.CharField(label="", widget=forms.TextInput(
        attrs = {
            "id":"email",
            "placeholder":"email"
        }
    ))
    status = forms.CharField(label="", widget=forms.Textarea(
        attrs = {
            "id":"status",
            "placeholder":"status....",
        }
    ))

    class Meta:
        model = StatusModel
        fields = ("email", "status")