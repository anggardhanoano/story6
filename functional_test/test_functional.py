from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions



class TestHome(StaticLiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome("./chromedriver",chrome_options=chrome_options)
        # self.browser = webdriver.Chrome()

    def tearDown(self):
        self.browser.close()
    
    def test_content(self):
        self.browser.get("http://127.0.0.1:8000/")
        time.sleep(5)
        elem = self.browser.find_element_by_css_selector("#greet")
        self.assertEqual(
            elem.find_element_by_tag_name("h1").text,
            "Hello,Welcome :)."
        )

    def test_input_data(self):
        self.browser.get("http://127.0.0.1:8000/")
        time.sleep(5)
        btn = self.browser.find_element_by_id("myBtn")
        btn.click()
        elem = self.browser.find_element_by_tag_name("form")
        email = elem.find_element_by_id("email")
        status = elem.find_element_by_id("status")
        email.send_keys("halo@gmail.com")
        status.send_keys("hahahahhaha")
        elem.submit()
        time.sleep(5)
        content = self.browser.find_element_by_css_selector(".status")
        self.assertIn(
            "halo@gmail.com",
            content.find_element_by_tag_name("p").text,
        )

    def test_go_to_biodata(self):
        self.browser.get("http://127.0.0.1:8000/")
        time.sleep(5)
        btn = self.browser.find_element_by_tag_name("a")
        btn.click()
        content = self.browser.find_element_by_css_selector(".content")
        self.assertEqual(
            "Anggardha Febriano",
            content.find_element_by_tag_name("h1").text
        ) 


    def test_go_to_home(self):
        self.browser.get("http://127.0.0.1:8000/biodata")
        btn = self.browser.find_element_by_tag_name("a")
        btn.click()
        time.sleep(5)
        content = self.browser.find_element_by_css_selector(".content")
        elem = self.browser.find_element_by_css_selector("#greet")
        self.assertEqual(
            elem.find_element_by_tag_name("h1").text,
            "Hello,Welcome :)."
        )        

    # Untuk Challenge
    def test_cek_font_size(self):
        self.browser.get("http://127.0.0.1:8000/")
        time.sleep(5)
        font = self.browser.find_element_by_css_selector("#greet")
        prop = font.value_of_css_property("overflow")
        self.assertEqual(prop, "hidden")

    def test_cek_font_size(self):
        self.browser.get("http://127.0.0.1:8000/")
        time.sleep(5)
        font = self.browser.find_element_by_css_selector("#greet")
        prop = font.value_of_css_property("overflow")
        self.assertEqual(prop, "hidden")

    def test_cek_button_size(self):
        self.browser.get("http://127.0.0.1:8000/")
        time.sleep(5)
        font = self.browser.find_element_by_css_selector(".button")
        prop = font.value_of_css_property("width")
        self.assertEqual(prop, "100px")

    def test_npm(self):
        self.browser.get("http://127.0.0.1:8000/biodata/")
        content = self.browser.find_element_by_css_selector(".content")
        npm = content.find_element_by_id("npm").text
        self.assertEqual(npm, "1806235800")

    def test_copyright(self):
        self.browser.get("http://127.0.0.1:8000/")
        time.sleep(5)
        footer = self.browser.find_element_by_id("footer")
        nama = footer.find_element_by_tag_name("p").text
        self.assertIn(nama,"Anggardha Febriano 2019")

    def test_theme(self):
        self.browser.get("http://127.0.0.1:8000/")
        time.sleep(5)
        btn = self.browser.find_element_by_css_selector(".theme")
        btn.click()
        color = self.browser.find_element_by_css_selector(".dark")
        prop = color.value_of_css_property("background-color")
        self.assertEqual(prop, "rgba(55, 55, 55, 1)")

    def test_accordion(self):
        self.browser.get("http://127.0.0.1:8000/biodata/")
        accordion = self.browser.find_element_by_css_selector(".accordion1")
        title = accordion.find_element_by_tag_name("p").text
        self.assertEqual(title, "Aktivitas")

class TestStory9(StaticLiveServerTestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome("./chromedriver",chrome_options=chrome_options)
        # self.browser = webdriver.Chrome()

    def tearDown(self):
        self.browser.close()

    def test_to_toko(self):
        self.browser.get("http://127.0.0.1:8000/")
        time.sleep(5)
        btn = self.browser.find_element_by_css_selector("#toko")
        btn.click()
        price = self.browser.find_element_by_css_selector("#total-price").text
        self.assertEqual("Total Price",price)


    